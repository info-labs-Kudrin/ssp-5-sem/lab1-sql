INSERT INTO LABORATORY(number,name,capacity,disposition) VALUES (1,'Физика плазмы',10,'МИФИ');
INSERT INTO LABORATORY(number,name,capacity,disposition) VALUES (2,'Нанотехнологии от кутюр',15,'2ой этаж РАН');
INSERT INTO LABORATORY(number,name,capacity,disposition) VALUES (3,'Процессы брожения',1,'подвал старого дома');

INSERT INTO STUDENT(id,name,year) VALUES (1,'Иванов И. И.',1);
INSERT INTO STUDENT(id,name,year) VALUES (2,'Петров П. П.',1);
INSERT INTO STUDENT(id,name,year) VALUES (3,'Сафаров И. С.',2);
INSERT INTO STUDENT(id,name,year) VALUES (4,'Живаго Ю. А.',4);
INSERT INTO STUDENT(id,name,year) VALUES (5,'Антипова Л. Ф.',4);

INSERT INTO WORK(number,name) VALUES (1,'Изучение плазмы в магнитном поле');
INSERT INTO WORK(number,name) VALUES (2,'Изучение плазмы в открытом поле');
INSERT INTO WORK(number,name) VALUES (3,'Изучения возгонки и конденсации этилового спирта');
INSERT INTO WORK(number,name) VALUES (4,'Нанокомпьютеры и нанопрограммисты');
INSERT INTO WORK(number,name) VALUES (5,'Чай с наночаинками');

INSERT INTO PLAN(studentId,workNum,labNum) VALUES (1,1,1);
INSERT INTO PLAN(studentId,workNum,labNum) VALUES (2,1,1);
INSERT INTO PLAN(studentId,workNum,labNum) VALUES (1,2,3);
INSERT INTO PLAN(studentId,workNum,labNum) VALUES (2,2,3);
INSERT INTO PLAN(studentId,workNum,labNum) VALUES (3,3,2);
INSERT INTO PLAN(studentId,workNum,labNum) VALUES (4,3,2);

INSERT INTO REPORT(studentId,workNum,file) VALUES (1,1,'Отчет1.txt');
INSERT INTO REPORT(studentId,workNum,file) VALUES (2,1,'Отчет_Петрова.txt');
INSERT INTO REPORT(studentId,workNum,file) VALUES (2,2,'Еще_отчет_Петрова.txt');
INSERT INTO REPORT(studentId,workNum,file) VALUES (4,3,'Стихотворения Юрия Живаго');